<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Custom\Ipay88;
use App\LogIpay88;
use App\Profile;
use App\Events\PaymentSuccessful;

class PaymentController extends Controller
{
  public function request(Request $request, $pro_id)
  {
    $profile = Profile::findOrFail($pro_id);

    if ($profile) {

      //Post request to payment gateway
      $ipay88 = new Ipay88;

      $data = [];
      $data['PaymentId'] = '';
      $data['RefNo'] = $profile->pro_id . '-' . date('YmdHis');
      $price = $data['Amount'] = !empty(env('OVERRIDE_PRICE')) ? env('OVERRIDE_PRICE') : $profile->category->cat_price;
      $data['ProdDesc'] = $profile->pro_category . ' - ' . $profile->category->cat_name;
      $data['UserName'] = $profile->pro_name;
      $data['UserEmail'] = $profile->pro_email;
      $data['UserContact'] = $profile->pro_phone;
      $data['Item'] = $pro_id;
      // $data['UserName'] = 'LC';
      // $data['UserEmail'] = 'lcyoong@gmail.com';
      // $data['UserContact'] = '0163236491';
      $data['Remark'] = '';

      $ipay88->data = $data;

      $data = $ipay88->prepare();

      return view('ipay88.request', compact('data', 'profile', 'price'));

    }


  }

  public function response(Request $request)
  {
    $post = $request->all();

    \Log::info($post);

    $err_desc = '';

    $ipay88 = new iPay88;

    $verified = 0;

    $signature = $ipay88->signature(config('ipay88.merchant_key') . array_get($post, 'MerchantCode')
            . array_get($post, 'PaymentId') . array_get($post, 'RefNo')
            . str_replace(array('.',','), '', array_get($post, 'Amount')) . array_get($post, 'Currency')
            . array_get($post, 'Status'));

    if($signature == array_get($post, 'Signature')) $verified = 1;

    $log = LogIpay88::where('loi8_ref', '=', array_get($post, 'RefNo'))->first();

    if ($log) {

      $log->update([
        'loi8_paymentid' => array_get($post, 'PaymentId'),
        'loi8_status' => array_get($post, 'Status'),
        'loi8_transid' => array_get($post, 'TransId'),
        'loi8_errstatus' => array_get($post, 'ErrStatus'),
        'loi8_errdesc' => array_get($post, 'ErrDesc'),
        'loi8_trandate' => array_get($post, 'TranDate'),
        'loi8_response' => utf8_encode(serialize($post)),
        'loi8_verified' => $verified,
      ]);

      $item = $log->loi8_item;

      if($verified && array_get($post, 'Status')) {

        // Update status
        $profile = Profile::findOrFail($item);

        event(new PaymentSuccessful($profile));

        // Send emails


        return view('ipay88.success');

      }
      else{

        $err_desc = array_get($post, 'ErrDesc');

        return view('ipay88.error', compact('err_desc', 'item'));
      }

    }

  }

  public function success()
  {

    return view('ipay88.success');

  }


  public function process_response($post, $type, &$err_desc = '', &$bill_id = ''){

    //--Verify signature
    $ipay88 = new iPay88;
    $verified = 0;
    $signature = $ipay88->signature(config('ipay88.merchant_key') . array_get($post, 'MerchantCode')
            . array_get($post, 'PaymentId') . array_get($post, 'RefNo')
            . str_replace(array('.',','), '', array_get($post, 'Amount')) . array_get($post, 'Currency')
            . array_get($post, 'Status'));
    if($signature == array_get($post, 'Signature')) $verified = 1;

    dd($post);
    //--Skip if transaction id exists (already processed)
    $logi8r = App::make('LogIpay88Response')->getByTransid(array_get($post, 'TransId'));
    //if(!is_null($logi8r)) return Response::json(['status'=>1, 'title'=>Lang::get('form.op_success_title'), 'message'=>Lang::get('form.op_update_success')]);

    //--Log ipay88 response
    $bill_id = substr(array_get($post, 'RefNo'), 0, strpos(array_get($post, 'RefNo'),'-'));
    $log = array();
    $log['loi8r_bill'] = $bill_id;
    $log['loi8r_status'] = array_get($post, 'Status');
    $log['loi8r_transid'] = array_get($post, 'TransId');
    $log['loi8r_errdesc'] = array_get($post, 'ErrDesc');
    $log['loi8r_paymentid'] = array_get($post, 'PaymentId');
    $log['loi8r_response'] = serialize($post);
    $log['loi8r_verified'] = $verified;
    $log['loi8r_type'] = $type;
    $log['loi8r_tx'] = array_get($post, 'RefNo');
    $result = App::make('LogIpay88Response')->insert($log);

    if(array_get($post, 'Status') == 1 && $verified && is_null($logi8r)){

    }

    if($verified && array_get($post, 'Status'))
      return 1;
    else{
      $err_desc = array_get($post, 'ErrDesc');
      return 0;
    }

  }

}
