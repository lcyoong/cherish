<?php

namespace App\Http\Requests;

class StoreProfile extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'pro_name' => 'required|max:191',
          'pro_email' => 'required|email',
          'pro_phone' => 'required',
          'pro_dob' => 'required|date',
          'pro_sex' => 'required',
          'pro_ic' => 'required',
          'pro_address' => 'required',
          'pro_zip' => 'required',
          'pro_city' => 'required',
          'pro_state' => 'required',
          'pro_emergency_name' => 'required',
          'pro_emergency_relation' => 'required',
          'pro_emergency_contact' => 'required',
          'pro_category' => 'required',
          'pro_size' => 'required'
        ];
    }
}
