<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('codes')->insert([
        ['cod_group' => 'state','cod_key' => 'SGR', 'cod_name' => 'Selangor', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'MLK', 'cod_name' => 'Melaka', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'TRG', 'cod_name' => 'Terengganu', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'SWK', 'cod_name' => 'Sarawak', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'SBH', 'cod_name' => 'Sabah', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'PLS', 'cod_name' => 'Perlis', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'PRK', 'cod_name' => 'Perak', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'PNG', 'cod_name' => 'Penang', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'PHG', 'cod_name' => 'Pahang', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'NSN', 'cod_name' => 'Negeri Sembilan', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'KTN', 'cod_name' => 'Kelantan', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'KDH', 'cod_name' => 'Kedah', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'JHR', 'cod_name' => 'Johor', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'KUL', 'cod_name' => 'Kuala Lumpur', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'LBN', 'cod_name' => 'Labuan', 'cod_order'=>1],
        ['cod_group' => 'state','cod_key' => 'PJY', 'cod_name' => 'Putra Jaya', 'cod_order'=>1],

        ['cod_group' => 'race_category','cod_key' => 'A', 'cod_name' => 'Junior M (RM30)', 'cod_order'=>1],
        ['cod_group' => 'race_category','cod_key' => 'B', 'cod_name' => 'Junior F (RM30)', 'cod_order'=>2],
        ['cod_group' => 'race_category','cod_key' => 'C', 'cod_name' => 'Standard M (RM60)', 'cod_order'=>3],
        ['cod_group' => 'race_category','cod_key' => 'D', 'cod_name' => 'Standard F (RM60)', 'cod_order'=>4],
        ['cod_group' => 'race_category','cod_key' => 'E', 'cod_name' => 'Senior M (RM60)', 'cod_order'=>5],
        ['cod_group' => 'race_category','cod_key' => 'F', 'cod_name' => 'Senior F (RM60)', 'cod_order'=>6],

        ['cod_group' => 'gender','cod_key' => 'M', 'cod_name' => 'Male', 'cod_order'=>1],
        ['cod_group' => 'gender','cod_key' => 'F', 'cod_name' => 'Female', 'cod_order'=>2],

      ]);

      DB::table('codes')->insert([
        ['cod_group' => 'size','cod_key' => 'S', 'cod_name' => 'S', 'cod_status' => 0, 'cod_order'=>1],
        ['cod_group' => 'size','cod_key' => 'M', 'cod_name' => 'M', 'cod_status' => 1, 'cod_order'=>2],
        ['cod_group' => 'size','cod_key' => 'L', 'cod_name' => 'L', 'cod_status' => 1, 'cod_order'=>3],
        ['cod_group' => 'size','cod_key' => 'XL', 'cod_name' => 'XL', 'cod_status' => 1, 'cod_order'=>4],
        ['cod_group' => 'size','cod_key' => 'XXL', 'cod_name' => 'XXL', 'cod_status' => 0, 'cod_order'=>5]
      ]);

      DB::table('categories')->insert([
        ['cat_code' => 'A','cat_name' => 'Junior Male (3KM)', 'cat_gender' => 'M', 'cat_price'=>30, 'cat_order'=>1, 'cat_agegroup' => 'J'],
        ['cat_code' => 'B','cat_name' => 'Junior Female (3KM)', 'cat_gender' => 'F', 'cat_price'=>30, 'cat_order'=>2, 'cat_agegroup' => 'J'],
        ['cat_code' => 'C','cat_name' => 'Adult Male (5KM)', 'cat_gender' => 'M', 'cat_price'=>60, 'cat_order'=>3, 'cat_agegroup' => 'A'],
        ['cat_code' => 'D','cat_name' => 'Adult Female (5KM)', 'cat_gender' => 'F', 'cat_price'=>60, 'cat_order'=>4, 'cat_agegroup' => 'A'],
        ['cat_code' => 'E','cat_name' => 'Veteran Male (3KM)', 'cat_gender' => 'M', 'cat_price'=>60, 'cat_order'=>5, 'cat_agegroup' => 'V'],
        ['cat_code' => 'F','cat_name' => 'Veteran Female (3KM)', 'cat_gender' => 'F', 'cat_price'=>60, 'cat_order'=>6, 'cat_agegroup' => 'V'],

      ]);

    }
}
