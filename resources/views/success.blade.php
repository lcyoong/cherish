@extends('layouts.master')
@section('content')
<section class="wow fadeIn xs-header-margin-top">
  <div class="container">
    <h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.thank_you')</h5>
    <div class="row">
      <div class="col-md-8 col-xs-12">
        <h6>@lang('general.register_success')</h6>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-xs-12">
        @include('partials.offline_payment', ['amount' => $profile->category->cat_price])
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-xs-12 margin-20px-tb">
        *Please check your email for the registration details. We look forward to meeting you on the day of the run.
      </div>
    </div>
	</div>
</section>
@stop
