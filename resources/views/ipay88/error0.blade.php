@extends('components.open')
@section('content_panel')
	<h1><div id="countdown"> </div></h1>
	<h3>{{$err_desc}}</h3>
	Please do not refresh this page. We will redirect you to the booking page.
	<script language="JavaScript">
	window.onload = function() {
	    var countdownElement = document.getElementById('countdown'),
	        seconds = 5,
	        second = 0,
	        interval;
	
	    interval = setInterval(function() {
	        countdownElement.firstChild.data = 'There was problem with your payment. @lang('form.redirecting') ' + (seconds - second) + ' seconds';
	        if (second >= seconds) {
	            window.location.href = "{{$redirect}}";
	            clearInterval(interval);
	        }
	
	        second++;
	    }, 1000);
	}	
	</script>		
@stop