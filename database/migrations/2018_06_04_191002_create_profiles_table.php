<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('pro_id');
            $table->string('pro_name');
            $table->string('pro_email');
            $table->string('pro_phone');
            $table->date('pro_dob')->nullable();
            $table->string('pro_ic')->nullable();
            $table->char('pro_sex', 1)->default('M');
            $table->text('pro_address')->nullable();
            $table->string('pro_zip')->nullable();
            $table->string('pro_city')->nullable();
            $table->string('pro_state')->nullable();
            $table->string('pro_emergency_name')->nullable();
            $table->string('pro_emergency_relation')->nullable();
            $table->string('pro_emergency_contact')->nullable();
            $table->string('pro_category');
            $table->char('pro_size', 2)->nullable();
            $table->string('pro_status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
