<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Ipay88Controller extends Controller
{
//--Prepare to redirect to request page
//--$bilid						Bill id
//--$amount						Amount
public function request($bilid, $amount = 0){

  //--Validate profile
  //$pay = App::make('Payment')->getByBill($bilid);
  $bill = App::make('Bill')->find($bilid);

  if($bill){
    $pro = App::make('Profile')->find($bill->bil_profile);
    $ipay88 = array();
    $ipay88['PaymentId'] = '';
    //$ipay88['RefNo'] = $bilid;
    $ipay88['RefNo'] = $bilid.'-'.date('YmdHis');
    $ipay88['Amount'] = number_format($amount + ($amount * Config::get('ipay88.processing_percent') / 100), 2, '.', '');
    //$ipay88['Amount'] = '1.00';
    $ipay88['Currency'] = Config::get('ipay88.default_currency');
    $ipay88['ProdDesc'] = $bill->bil_description;
    $ipay88['UserName'] = $pro->pro_name;
    $ipay88['UserEmail'] = $pro->pro_email;
    $ipay88['UserContact'] = $pro->pro_contact1;
    $ipay88['Remark'] = '';
    $ipay88['MerchantCode'] = Config::get('ipay88.merchant_code');
    $ipay88['ResponseURL'] = URL::to(Config::get('ipay88.response_path'));
    $ipay88['BackendURL'] = URL::to(Config::get('ipay88.backend_path'));
    $ipay88['Lang'] = Config::get('ipay88.encoding');
    $ipay88['signature'] = Ipay88::iPay88_signature(Config::get('ipay88.merchant_key') . $ipay88['MerchantCode'] . $ipay88['RefNo'] . str_replace(array('.',','), '', $ipay88['Amount']) . $ipay88['Currency']);

    //--Log ipay88 request
    $log = array();
    $log['loi8_bill'] = $bilid;
    $log['loi8_tx'] = $ipay88['RefNo'];
    $log['loi8_request'] = serialize($ipay88);
    $result = App::make('LogIpay88')->insert($log);
    $response = MyHelpers::jsonResponse($result, $err);

    //--Display redirecting page
    if(!$err){
      $options = array('ipay88' => $ipay88);
      $options = $options + $this->def_options;
      return View::make('ipay88.request', $options);
    }
  }
}

//--Process response from ipay88
public function response(){

  $post = Input::all();
  $err_desc = '';
  $bill_id = '';
  $fate = $this->process_response($post, 'response', $err_desc, $bill_id);

  //--Success: Go to success page
  if($fate){
    //--Clear booking session
    // Session::forget('room_search');
    // Session::forget('paynowitems');
    // Session::forget('facility');
    //$options = array('redirect' => URL::to('/member/dashboard'));
    $options = array('redirect' => URL::to($this->def_options['urlp'] . '/bill/pay/' . $bill_id));
    $options = $options + $this->def_options;

    // Event::queue('ipay88.paid');

    return View::make('ipay88.success', $options);
  //--Failed: Go back to preview page
  }else{
    //$options = array('err_desc' => $err_desc, 'redirect' => URL::to($this->def_options['urlp'] . '/booking/new/preview'));
    $options = array('err_desc' => $err_desc, 'redirect' => URL::to($this->def_options['urlp'] . '/bill/pay/' . $bill_id));
    $options = $options + $this->def_options;
    return View::make('ipay88.error', $options);
    //return Redirect::to($this->def_options['urlp'] . '/booking/new/preview')->with('error', $custom_err);
  }

}

//--Process backend from ipay88
public function backend(){

  $post = Input::all();
  $this->process_response($post, 'backend');

  //--Return a standard message for ipay88
  $options = ['output' => Config::get('ipay88.backend_ok')];
  return View::make('layouts.blank', $options);

}

/**
 * Mocking ipay88 processor
 * @return [type] [description]
 */
public function mock()
{
  $request = Input::all();

  $request['PaymentId'] = 2;
  $request['TransId'] = 'T' . rand(1000000000, 9999999999);
  $request['AuthCode'] = '343434';
  $request['Status'] = 1;
  $request['ErrDesc'] = '';
  $request['CCNo'] = '1234567890';
  $request['CCName'] = $request['UserName'];
  $request['S_bankname'] = 'Acme Ltd';
  $request['S_country'] = 'Utopia';
  $request['TokenId'] = '';
  $request['MockSignature'] = Ipay88::iPay88_signature(Config::get('ipay88.merchant_key') . array_get($request, 'MerchantCode')
          . array_get($request, 'PaymentId') . array_get($request, 'RefNo')
          . str_replace(array('.',','), '', array_get($request, 'Amount')) . array_get($request, 'Currency')
          . array_get($request, 'Status'));

  $options = [
    'ipay88' => $request,
  ];

  $options = $options + $this->def_options;

  return View::make('ipay88.mock', $options);
}

//--Process response method
//--Return 1 = success, 0 = error
public function process_response($post, $type, &$err_desc = '', &$bill_id = ''){

  //--Verify signature
  $verified = 0;
  $signature = Ipay88::iPay88_signature(Config::get('ipay88.merchant_key') . array_get($post, 'MerchantCode')
          . array_get($post, 'PaymentId') . array_get($post, 'RefNo')
          . str_replace(array('.',','), '', array_get($post, 'Amount')) . array_get($post, 'Currency')
          . array_get($post, 'Status'));
  if($signature == array_get($post, 'Signature')) $verified = 1;

  //--Skip if transaction id exists (already processed)
  $logi8r = App::make('LogIpay88Response')->getByTransid(array_get($post, 'TransId'));
  //if(!is_null($logi8r)) return Response::json(['status'=>1, 'title'=>Lang::get('form.op_success_title'), 'message'=>Lang::get('form.op_update_success')]);

  //--Log ipay88 response
  $bill_id = substr(array_get($post, 'RefNo'), 0, strpos(array_get($post, 'RefNo'),'-'));
  $log = array();
  $log['loi8r_bill'] = $bill_id;
  $log['loi8r_status'] = array_get($post, 'Status');
  $log['loi8r_transid'] = array_get($post, 'TransId');
  $log['loi8r_errdesc'] = array_get($post, 'ErrDesc');
  $log['loi8r_paymentid'] = array_get($post, 'PaymentId');
  $log['loi8r_response'] = serialize($post);
  $log['loi8r_verified'] = $verified;
  $log['loi8r_type'] = $type;
  $log['loi8r_tx'] = array_get($post, 'RefNo');
  $result = App::make('LogIpay88Response')->insert($log);

  if(array_get($post, 'Status') == 1 && $verified && is_null($logi8r)){

    //$bill_id = array_get($post, 'RefNo');
    //$bill_id = substr(array_get($post, 'RefNo'), 0, strpos(array_get($post, 'RefNo'),'-'));
    DB::beginTransaction();
    //--Update bill record
    //$bill = array();
    $bill = App::make('Bill')->find($bill_id)->toArray();
    //$bill['bil_id'] = $bill_id;
    //$bill['bil_paid'] = $bill['bil_paid'] + array_get($post, 'Amount');
    //if($bill['bil_paid'] >= ($bill['bil_gross'] + $bill['bil_tax'])) $bill['bil_status'] = Config::get('mystatus.paid');
    //$instance = App::make('Bill')->process($bill);	-- 20160710 - Bill is automatically updated after payment is done.
    //$response = MyHelpers::jsonResponse($instance, $err);
    //if($err) return $response;

    //--Create payment record
    //$pay = App::make('Payment')->getByBill($bill_id)->toArray();
    $amount_paid = str_replace(',', '', array_get($post, 'Amount'));

    $pay = array();
    $pay['pay_status'] = Config::get('mystatus.paid');
    $pay['pay_ref'] = array_get($post, 'TransId');
    $pay['pay_profile'] = $bill['bil_profile'];
    $pay['pay_amount'] = $amount_paid / (1 + Config::get('ipay88.processing_percent') / 100);
    $pay['pay_method'] = 'ipay88';
    $pay['pay_bill'] = $bill_id;
    $pay['pay_date'] = date(Config::get('myapp.date_format_vld'));
    $instance = App::make('Payment')->process($pay);

    Event::fire('ipay88.paid', $instance);

    //--Update booking record (if pending)
    $book_id = $bill['bil_booking'];
    $instance_book = App::make('Booking')->find($book_id);
    //dd($instance_book->book_status);
    if($instance_book && $instance_book->book_status == Config::get('mystatus.pending')){
      $book = array();
      $book['book_id'] = $book_id;
      $book['book_status'] = Config::get('mystatus.inprocess');
      $instance = App::make('Booking')->process($book);
      //$response = MyHelpers::jsonResponse($instance, $err);

      //--Create a checkin bill
      //App::make('Bill')->processCheckin($book_id);

      //--Send email: Booking successful
      App::make('EmailAlert')->sendNewBooking($book_id, $this->def_options);
    }

    //--Create the next bill
    DB::commit();
  }
  if($verified && array_get($post, 'Status'))
    return 1;
  else{
    $err_desc = array_get($post, 'ErrDesc');
    return 0;
  }

}
}
