<h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.tnc')</h5>
<p>You represent and warrant to Bharat Group including all its subsidiaries and affiliated entities (“Bharat Group”) that you have the authority to register and/or act as agent to complete the registration of the Events on behalf of yourself and/or any party(s) you are registering (“Registered Parties”) through our online registration portal, runforcherish.bharattea.com.my. Your acts for and on behalf of the Registered Parties shall be binding upon them as if they have done it themselves.</p>

<p>You further warrant that you have the full authority to utilize the credit/debit card or online bank transfer to which registration fees and other transaction fees (if any) are to be charged. You also hereby indemnify Bharat Group of any and all claims of loss of damage related to the credit card and debit card transactions done over this registration portal.</p>

<div class="panel-group accordion-style1" id="accordion-design">
  <!-- start accordion item -->
  <div class="panel">
      <div class="panel-heading">
          <a data-toggle="collapse" data-parent="#accordion-design" href="#design1" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-extra-dark-gray text-uppercase">Registration Rules<span class="pull-right"><i class="ti-plus"></i></span></div></a>
      </div>
      <div id="design1" class="panel-collapse collapse" aria-expanded="false" role="tablist" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>All participants are to ensure that all information submitted by them for the purpose of registration are complete and accurate.</li>
              <li>The confirmation of participation is ONLY VALID when payment of the registration fees is successfully made.</li>
              <li>An event registration confirmation email will be sent upon successful payment. If you do not receive the confirmation email within 7 working days from the successful registration date, please contact our hotline or email us at runforcherish@bharattea.com.my</li>
              <li>The confirmation email is the ONLY VALID proof of confirmed registration and is to be used for the collection of race kits / participation.</li>
              <li>By confirmation of payment, the participant agrees to abide by the rules and regulations set forth by the Organiser.</li>
              <li>All entries are NOT TRANSFERABLE, REFUNDABLE NOR DEFERRABLE.</li>
              <li>Upon confirmation of participation, changes to the category or other details will not be entertained.</li>
              <li>The Organiser reserves the right to limit, accept or reject any entries without prior notice and reasons.</li>
              <li>The Organiser reserves the right to close entries before deadline without any notice or when a quota of an event is full.</li>
              <li>The Organiser reserves the right to change the event date or venue at their own discretion.</li>
              <li>The Organiser reserves the right to not refund any participants for their registration to The Event, in the event that the participant is unable to attend The Event for whatever reason.</li>
              <!-- <li>Non-Malaysian participants must ensure that they obtain all necessary entry, exit, health and other documents. Please contact the applicable Consulate or Embassy for these additional requirements.</li> -->
              <li>All transactions through runforcherish.bharattea.com.my will be in Ringgit Malaysia (MYR or RM). For participants paying from overseas or using a non-Malaysian Bank, exchange rates conversion will apply (depending on individual banks). If you have any questions on the bank rates, please contact your local banks.</li>
            </ul>
          </div>
      </div>
  </div>
  <!-- end accordion item -->
  <!-- start accordion item -->
  <div class="panel">
      <div class="panel-heading">
          <a data-toggle="collapse" data-parent="#accordion-design" href="#design2" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-extra-dark-gray text-uppercase">Personal Data Protection Act<span class="pull-right"><i class="ti-plus"></i></span></div></a>
      </div>
      <div id="design2" class="panel-collapse collapse" aria-expanded="false" role="tablist" style="height: 0px;">
          <div class="panel-body">
            <p>By providing any information, personal or otherwise of yourself or other person who you are registering for, in order to sign up for the Event or any activity(ies) involving the Organiser and the Event sponsors (if any), you and /or any other person who you are registering for are deemed to have agreed to the following:</p>

            <p>The Personal Data of the Registered Parties shall be used by the Organiser and Bharat Group for the purposes of sending communication materials to the Registered Parties, including but not limited to, direct mailers, emails, Short Messaging Service and/or telephone calls.</p>

            <p>The Organiser may engage the services of third parties to process the Registered Parties’ Personal Data. All such third parties are contractually obliged not to use the Registered Parties’ Personal Data for any other purpose, other than as specified herein.</p>

            <p>Any of the Registered Parties has the right, at any time, to:</p>

            <p>withdraw his/her consent for the use of the Personal Data;</p>

            <p>to request for any correction/update of the Personal Data; or</p>

            <p>raise any general queries regarding the use of his/her Personal Data;</p>

            <p>by contacting the Organiser directly and Bharat Group by sending an email to  Organiser’s email and contact numbers are listed in the event detail</p>
          </div>
      </div>
  </div>
  <!-- end accordion item -->
  <!-- start accordion item -->
  <div class="panel">
      <div class="panel-heading active-accordion">
          <a data-toggle="collapse" data-parent="#accordion-design" href="#design3" class="" aria-expanded="true"><div class="panel-title font-weight-500 text-extra-dark-gray text-uppercase">Refunds<span class="pull-right"><i class="ti-minus"></i></span></div></a>
      </div>
      <div id="design3" class="panel-collapse collapse" aria-expanded="true" role="tablist" style="">
          <div class="panel-body">
            It is the responsibility of the Organizer to communicate its refund policy to the Registered Parties and to issue refunds to the Registered Parties via the Site or otherwise. If the Registered Parties desires to request a refund, the Registered Parties must request the refund from the Organizer. All communications or disputes regarding refunds are between the Organizer and the Registered Parties and Bharat Group will not be responsible or liable in any way for refunds, errors in issuing refunds, or lack of refunds in connection with the Services. All communications and disputes regarding chargebacks and refunds are between the Organizer and the Registered Parties and Bharat Group will not be responsible or liable in any way for chargebacks in connection with the Registered Parties' use of the Services. If you are the Registered Parties and you wish to request a refund in connection with an event listed on the Services, you should contact the applicable Organizer directly. The Registered Parties will need to contact Bharat Group at runforcherish@bharattea.com.my if they were to contact the Organizer for refund.
          </div>
      </div>
  </div>
  <!-- end accordion item -->
  <!-- start accordion item -->
  <div class="panel">
      <div class="panel-heading">
          <a data-toggle="collapse" data-parent="#accordion-design" href="#design4" class="collapsed" aria-expanded="false"><div class="panel-title font-weight-500 text-extra-dark-gray text-uppercase">Waiver of Liability<span class="pull-right"><i class="ti-plus"></i></span></div></a>
      </div>
      <div id="design4" class="panel-collapse collapse" aria-expanded="false" role="tablist">
          <div class="panel-body">
            <p>The Registered Parties acknowledges and agrees that there may be certain danger and risk involved in the Event which may not be foreseeable, including without limitation to personal injury, property damage or death.</p>

            <p>The Registered Parties acknowledges and declares that:</p>

            <p>the Organiser, their subcontractors, agents, directors, employees;</p>

            <p>com, their subcontractors, agents, directors, employees, advisers;</p>

            <p>all municipal agencies and local governments whose property or personnel are used;</p>

            <p>all sponsors (including co-sponsors), their subcontractors, agents, directors, employees; and</p>

            <p>any individuals related to the Event, volunteers and affiliates;</p>

            <p>(collectively referred to as the, “Releasees”);</p>

            <p>shall not be liable to the Registered Parties for any liability in contract, tort (including negligence) or otherwise for any loss of business, revenue or profits, anticipated savings or wasted expenditure, loss or damage of or to personal equipment belonging to the Registered Parties or any indirect or consequential loss or damage whatsoever (notwithstanding that such loss was within the contemplation of the parties at the date of submitting his/her registration) arising out of the Registered Parties taking part in the Event or any other matter arising under these Conditions nor for an aggregate amount greater than the Fee paid by the Registered Parties.</p>

            <p>Without limiting the foregoing, the Releasees will not be liable for any actions of any spectators or other third parties unless otherwise set out herein.</p>

            <p>The Registered Parties hereby expressly and irrevocably renounces, for him/herself, heirs and successors in title the right to any recourse or claims whatsoever against the Releasees, or persons for whom it is answerable (“Associated Parties”) as a result of any kind of physical, mental or other loss or damage of whatsoever nature (including any loss of earnings, profits, or pain and suffering) suffered by the Registered Parties, directly or indirectly, or by his/her family or by any dependents and caused by an event in any way relating to the Registered Parties’s participation in Event.</p>

            <p>The Registered Parties hereby undertakes, for him/herself, heirs and successors in title to indemnify and hold harmless the Organiser, and the Associated Parties for any costs and/or amount, which they or any of them may be required to pay as a result of any aforesaid recourse or claim by whomsoever made. The Registered Parties also agrees to indemnify the Releasees and the Associated Parties for any claim, actions, liabilities or losses resulting from any breach of the Registered Parties’s declarations above and/or the Registered Parties’s negligent acts or omissions and/or wilful misconduct.</p>

            <p>The Registered Parties hereby release, waive, discharge and covenant not to sue Howei for any and all liability from any and all claims arising from the participation in the events by you or any other registered party.</p>

            <p>The Registered Parties acknowledges that participation in the event will be physically demanding and warrants that he/she is in good physical conditionand have sufficient preparations and is able to safely participate in the Events. The Registered Parties are fully aware of the nature of the event and the associated medical and physical risks and hazards involved in participating in the Events, including the possibilities of serious physical trauma, injury or death.</p>

            <p>The Registered Parties consents to the use of his/her name, photographs taken and any appearance filmed or recorded by Bharat  Group in broadcasts, newspapers, magazines, brochures, internet website, and other media for the purpose of advertising, publicity and otherwise in relation to the exploitation of the event. Registered Parties further agrees that he/she shall not claim or demand for any compensation or payment for such usage.</p>

            <p>The Registered Parties acknowledges that any payment of the fee and transaction costs (if any) for the events, is non-refundable, non-transferable and non-deferrable.</p>

            <p>The Registered Parties acknowledges that the Organizer of the Event has the right to alter, change, cancel and/or postpone any of these events as a result of circumstances that would direct effect or impact the event which is or beyond control.</p>

            <p>Bharat Group makes no warranty and assurance that the services in  http://runforcherish.bharattea.com.my/portal will be uninterrupted, secure or error free. Howei also does not guarantee the accuracy or completeness of any information, or provided in connection to the Events or otherwise in the website.</p>

            <p>The Registered Parties agree that this Agreement and Waiver is governed and construed in accordance with the laws of Malaysia. The Registered Parties further agree that this Agreement and Waiver is intended to be as broad and inclusive as is permitted by law of Malaysia.</p>

            <p>The Registered Parties affirm that they have read, understood and fully agreed to the terms and waiver, and they will give up substantial rights, including the rights to sue and to release all liability to the greatest extend allowed by law.</p>
          </div>
      </div>
  </div>
  <!-- end accordion item -->
</div>
