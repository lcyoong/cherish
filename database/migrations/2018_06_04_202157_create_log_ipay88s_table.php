<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogIpay88sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ipay88s', function (Blueprint $table) {
            $table->increments('loi8_id');
            $table->integer('loi8_item')->default(0);
            $table->string('loi8_ref');
            $table->string('loi8_transid')->nullable();
            $table->string('loi8_status')->nullable();
            $table->string('loi8_errdesc')->nullable();
            $table->string('loi8_errstatus')->nullable();
            $table->string('loi8_paymentid')->nullable();
            $table->string('loi8_trandate')->nullable();
            $table->text('loi8_request')->nullable();
            $table->text('loi8_response')->nullable();
            $table->boolean('loi8_verified')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ipay88s');
    }
}
