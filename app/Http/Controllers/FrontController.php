<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProfile;
use Custom\Ipay88;
use App\Profile;
use App\Code;
use App\Events\PaymentSuccessful;

class FrontController extends Controller
{
  public function welcome()
  {
    return view('welcome');
  }

  public function index()
  {
    $states = Code::dropDown('state');

    $categories = Code::dropDown('race_category');

    $sizes = Code::dropDown('size');

    $genders = Code::dropDown('gender');

    $inner_link = true;

    $vdata = compact('states', 'categories', 'sizes', 'genders', 'inner_link');

    return view('index', $vdata);
  }

  public function register(StoreProfile $request)
  {
    $pro_data = $request->input();

    $pro_data['pro_status'] = 'pending';

    $profile = Profile::create($pro_data);

    if ($profile) {

      event(new PaymentSuccessful($profile));

      return $this->goodResponse('Registered successfully', $profile->pro_id);

      // $vdata = compact('profile');
      //
      // return view('success', $vdata);
      // return redirect("payment/{$pro->pro_id}");

    } else {


    }
  }

  public function success($id)
  {
    $profile = Profile::findOrFail($id);

    $vdata = compact('profile');

    return view('success', $vdata);
  }

  /**
    * Format a default good response
    * @param  string $message
    * @param  array $data
    * @return Response
    */
   private function goodResponse($message = '', $data = null, $cookie = null)
   {
     $response = response([
       'success' => true,
       'data' => $data,
       'message' => $message ? $message : trans('message.process_successful')
     ]);

     if (!is_null($cookie)) {
       return $response->cookie($cookie);
     }

     return $response;
   }
  // public function pay()
  // {
  //   //Save model
  //   // $pro = Profile::create();
  //
  //   //Post request to payment gateway
  //   $ipay88 = new Ipay88;
  //
  //   $data = [];
  //   $data['PaymentId'] = '';
  //   $data['RefNo'] = date('YmdHis');
  //   $data['Amount'] = 1;
  //   $data['ProdDesc'] = 'Prod Desc';
  //   // $data['UserName'] = $pro->pro_name;
  //   // $data['UserEmail'] = $pro->pro_email;
  //   // $data['UserContact'] = $pro->pro_contact1;
  //   $data['UserName'] = 'LC';
  //   $data['UserEmail'] = 'lcyoong@gmail.com';
  //   $data['UserContact'] = '0163236491';
  //   $data['Remark'] = '';
  //
  //   $ipay88->data = $data;
  //
  //   $data = $ipay88->prepare();
  //
  //   return view('ipay88.request', compact('data'));
  //
  // }

}
