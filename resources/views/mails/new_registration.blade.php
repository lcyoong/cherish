<h1>Thank you! You’ve registered to RunForCherish</h1>
<table>
  <tbody>
    <tr>
      <td>@lang('profile.pro_id')</td>
      <td>:</td>
      <td>{{ $profile->pro_id }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_name')</td>
      <td>:</td>
      <td>{{ $profile->pro_name }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_email')</td>
      <td>:</td>
      <td>{{ $profile->pro_email }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_dob')</td>
      <td>:</td>
      <td>{{ Carbon\Carbon::parse($profile->pro_dob)->format('d-m-Y') }} (@lang('general.age') {{ Carbon\Carbon::parse($profile->pro_dob)->age }})</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_ic')</td>
      <td>:</td>
      <td>{{ $profile->pro_ic }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_sex')</td>
      <td>:</td>
      <td>{{ $profile->pro_sex }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_address')</td>
      <td>:</td>
      <td>{{ $profile->pro_address }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_zip')</td>
      <td>:</td>
      <td>{{ $profile->pro_zip }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_city')</td>
      <td>:</td>
      <td>{{ $profile->pro_city }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_state')</td>
      <td>:</td>
      <td>{{ $profile->pro_state }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_emergency_name')</td>
      <td>:</td>
      <td>{{ $profile->pro_emergency_name }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_emergency_relation')</td>
      <td>:</td>
      <td>{{ $profile->pro_emergency_relation }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_emergency_contact')</td>
      <td>:</td>
      <td>{{ $profile->pro_emergency_contact }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_category')</td>
      <td>:</td>
      <td>{{ $profile->pro_category }} - {{ $profile->category->cat_name }}</td>
    </tr>
    <tr>
      <td>@lang('category.cat_price')</td>
      <td>:</td>
      <td>RM{{ $profile->category->cat_price }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_size')</td>
      <td>:</td>
      <td>{{ $profile->pro_size }}</td>
    </tr>
    <tr>
      <td>@lang('profile.pro_status')</td>
      <td>:</td>
      <td><b>{{ $profile->pro_status }}</b></td>
    </tr>
    <tr>
      <td>@lang('profile.created_at')</td>
      <td>:</td>
      <td>{{ $profile->created_at }}</td>
    </tr>
  </tbody>
</table>
<p>
@include('partials.offline_payment', ['amount' => $profile->category->cat_price])
</p>
<p>We look forward to meeting you on the Run day.</p>
@include('partials.support')
