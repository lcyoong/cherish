<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->char('cat_code', 1);
            $table->string('cat_name');
            $table->char('cat_gender', 1);
            $table->char('cat_agegroup', 1);
            $table->decimal('cat_price', 15,2)->default(0);
            $table->char('cat_status', 1)->default(1);
            $table->integer('cat_order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
