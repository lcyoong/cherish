@extends('layouts.master')
@section('content')
<!-- start hero section -->
<section id="home" class="no-padding parallax mobile-height wow fadeIn" data-stellar-background-ratio="0.5" style="background-image:url('{{ asset('img/bg-hero-plantation.jpg') }}');">
    <div class="opacity-very-light bg-extra-dark-gray"></div>
    <div class="container position-relative full-screen">
      <div class="slider-typography">
          <div class="slider-text-middle-main">
              <div class="slider-text-bottom">
                <a class="single-image-lightbox" href="{{ asset('img/poster.jpg') }}"><img class="xs-display-none" src="{{ asset('img/poster.jpg') }}" alt="" data-no-retina="" width="300"></a>
                <div class="col-lg-6 col-md-7 col-sm-12 col-xs-12 pull-right bg-deep-pink-opacity padding-six-lr md-padding-seven-lr padding-five-tb xs-padding-30px-all last-paragraph-no-margin">
                    <!-- <div class="box-separator-line width-180px bg-white md-width-120px sm-width-90px sm-display-none"></div> -->
                    <a class="single-image-lightbox" href="{{ asset('img/poster.jpg') }}"><img class="float-left mobile-only" src="{{ asset('img/poster.jpg') }}" alt="" data-no-retina="" width="100" style="margin-right: 15px;"></a>
                    <h1 class="font-weight-600 alt-font text-white width-95 sm-width-100">Run For Cherish 2018</h1>
                    <p class="text-large font-weight-600 text-white width-75 md-width-85 sm-width-95 xs-width-100">4 Aug 2018 | <i class="fa fa-map-marker"></i> Cameron Valley Tea Plantation</p>
                    <a href="#register" class="inner-link btn btn-medium btn-white margin-40px-top text-link-deep-pink xs-margin-10px-top">@lang('general.register')</a>
                </div>
              </div>
          </div>
      </div>
        <!-- <div class="slider-typography text-center">
            <div class="slider-text-middle-main xs-padding-15px-lr">
                <div class="slider-text-middle">
                  <div class="col-lg-8 col-sm-10 col-lg-push-4 col-sm-push-1 text-left bg-white-opacity padding-seven-all md-padding-ten-all sm-text-center">
                      <div class="alt-font text-extra-dark-gray text-uppercase font-weight-700 letter-spacing-minus-2 title-large">Run for Cherish</div>
                      <div class="separator-line-horrizontal-full width-100 margin-35px-tb sm-margin-25px-tb xs-margin-20px-tb bg-extra-dark-gray"></div>
                      <div class="alt-font text-extra-dark-gray text-uppercase font-weight-700 letter-spacing-minus-2 text-large">Cameron Valley Tea Challenge</div>
                  </div>
                  <div class="down-section text-center"><a href="#register" class="inner-link"><i class="ti-arrow-down icon-extra-small text-white bg-deep-pink padding-15px-all xs-padding-10px-all border-radius-100"></i></a></div>
                </div>
            </div>
        </div> -->
    </div>
</section>
<!-- end hero section -->

<!-- start feature box section -->
<section id="register" class="wow fadeIn">
  <div class="container">

    @include('partials.fb_share')
    @include('partials.register_closed')
  </div>
</section>
<!-- end feature box section -->

<!-- start feature box section -->
<section id="tnc" class="bg-very-light-gray wow fadeIn">
  <div class="container">
    @include('partials.tnc')
  </div>
</section>
<!-- end feature box section -->

@stop
