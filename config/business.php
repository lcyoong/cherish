<?php

//--Configuration for status values
return array(
  'project' => 'RunForCherish',
	'organization' => 'Bharat Group',
  'email' => 'run4cherish@gmail.com',
  'tel1' => '+6013 520 5151 (Datin Aseema)',
  'tel2' => '+6019 890 7056 (Durai)',
);
