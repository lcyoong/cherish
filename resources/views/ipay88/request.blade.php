@extends('layouts.master')
@section('content')

<section class="wow fadeIn xs-header-margin-top">
  <div class="container">
    <h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.payment')</h5>
    <div class="row">
      <div class="col-md-2 col-xs-4">@lang('profile.pro_name')</div>
      <div class="col-md-10 col-xs-8">{{ $profile->pro_name }}</div>
    </div>
    <!-- <div class="row">
      <div class="col-md-2 col-xs-4">@lang('profile.pro_sex')</div>
      <div class="col-md-10 col-xs-8">{{ $profile->pro_sex }}</div>
    </div> -->
    <!-- <div class="row">
      <div class="col-md-2 col-xs-4">@lang('profile.pro_dob')</div>
      <div class="col-md-10 col-xs-8">{{ $profile->pro_dob }} (@lang('general.age') {{ Carbon\Carbon::parse($profile->pro_dob)->age }})</div>
    </div> -->
    <div class="row">
      <div class="col-md-2 col-xs-4">@lang('profile.pro_category')</div>
      <div class="col-md-10 col-xs-8">{{ $profile->pro_category }} - {{ $profile->category->cat_name }}</div>
    </div>
    <div class="row">
      <div class="col-md-2 col-xs-4">@lang('profile.pro_status')</div>
      <div class="col-md-10 col-xs-8"><b>{{ $profile->pro_status }}</b></div>
    </div>
    <div class="row">
      <div class="col-md-2 col-xs-4">@lang('category.cat_price')</div>
      <div class="col-md-10 col-xs-8">RM{{ $price }}</div>
    </div>
    <br/>
		<img src="{{ asset('img/logo-ipay88.png') }}" width="80"><br/>
		By clicking on the button below, you will be transferred to a secured online payment gateway.
		<form action="{{ env('IPAY88_ACTIVE') ? config('ipay88.request_url') : config('ipay88.mock_request_url') }}" method="post" name="request">
		<?php
		foreach ($data as $key => $value) {
			echo "<input type='hidden' name='".htmlentities($key)."' value='".htmlentities($value)."'>";
		}
		?>
		{{ Form::submit(trans('general.button_payment'), ['class' => 'btn btn-transparent-dark-gray btn-large margin-20px-top']) }}
		</form>
	</div>
</section>
@stop
