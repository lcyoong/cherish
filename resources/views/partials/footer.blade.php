<!-- start footer -->
<footer class="footer-modern-dark bg-extra-dark-gray padding-five-tb xs-padding-30px-tb">
    <div class="footer-widget-area padding-40px-bottom xs-padding-30px-bottom">
        <div class="container">
            <div class="row equalize xs-equalize-auto">
                <!-- start slogan -->
                <div class="col-md-4 col-sm-12 col-xs-12 xs-text-center sm-margin-three-bottom xs-margin-20px-bottom display-table" style="height: 72px;">
                    <div class="display-table-cell vertical-align-middle">
                        <h6 class="text-deep-pink width-70 md-width-100 no-margin-bottom"><i class="fa fa-heart"></i> {{ config('business.project') }}</h6>
                    </div>
                </div>
                <!-- end slogan -->
                <!-- start contact information -->
                <div class="col-md-8 col-sm-12 col-xs-12 xs-text-center xs-margin-20px-bottom display-table" style="height: 72px;">
                    <div class="display-table-cell vertical-align-middle">
                        <!-- <span class="display-block">301 The Greenhouse,<br>Custard Factory, London, E2 8DY.</span> -->
                        <i class="fa fa-envelope"></i> <a href="mailto:{{ config('business.email') }}" title="{{ config('business.email') }}">{{ config('business.email') }}</a>
                        <br/><i class="fa fa-mobile"></i> {{ config('business.tel1') }} | {{ config('business.tel2') }}
                    </div>
                </div>
                <!-- end contact information -->
                <!-- start social media -->
                <!-- <div class="col-md-4 col-sm-6 col-xs-12 social-style-2 xs-text-center display-table" style="height: 72px;">
                    <div class="display-table-cell vertical-align-middle">
                        <div class="social-icon-style-8">
                            <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                <li class="display-inline-block margin-10px-right"><a href="http://twitter.com" target="_blank" title="Twitter">Twitter</a></li>
                                <li class="display-inline-block margin-10px-right"><a href="http://facebook.com" target="_blank" title="Facebook">Facebook</a></li>
                                <li class="display-inline-block margin-10px-right"><a href="http://instagram.com" target="_blank" title="Instagram">Instagram</a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- end social media -->
            </div>
        </div>
    </div>
    <div class="container">
        <!-- start copyright -->
        <div class="footer-bottom border-top border-color-medium-dark-gray padding-40px-top xs-padding-30px-top">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">© 2018 {{ config('business.project') }} | {{ config('business.organization') }}</a></div>
            </div>
        </div>
        <!-- end copyright -->
    </div>
</footer>
<!-- end footer -->
<!-- start scroll to top -->
<a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
<!-- end scroll to top  -->
