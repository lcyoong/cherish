<?php

namespace App\Listeners;

use App\Events\PaymentSuccessful;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\NewRegistrationAlert;
use Illuminate\Support\Facades\Mail;

class EmailProfile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentSuccessful  $event
     * @return void
     */
    public function handle(PaymentSuccessful $event)
    {
      $profile = $event->profile;

      Mail::to($profile->pro_email)->bcc(explode(',',env('BCC_ADMIN')))->send(new NewRegistrationAlert($profile));
    }
}
