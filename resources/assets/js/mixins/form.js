module.exports = {
  data: {
    waiting: false,
    trigger: null,
    original_text: null,
    original_value: null,
  },

  methods: {

    startWait: function () {
      this.waiting = true
      this.original_text = this.trigger.innerHTML
      this.original_value = this.trigger.value
      this.trigger.innerHTML = "Processing..."
      this.trigger.value = "Processing..."
      // $("#ajax_loader").show();
    },

    endWait: function () {
      this.waiting = false
      this.trigger.innerHTML = this.original_text
      this.trigger.value = this.original_value
      // $("#ajax_loader").hide();
    },

    processWait: function (e) {
      this.trigger = e.target
    }

  }
}
