<!-- start header -->
<header>
    <!-- start navigation -->
    <nav class="navbar navbar-default bootsnav navbar-fixed-top header-light nav-box-width bg-black-opacity-light white-link">
        <div class="container nav-header-container">
            <div class="row">
                <!-- start logo -->
                <div class="col-md-2 col-xs-5">
                    <a href="{{ url('/') }}" title="Bharat Group" class="logo"><img src="{{ asset('img/logo-bharattea.png') }}" data-rjs="{{ asset('img/logo-bharattea.png') }}" class="logo-dark" alt="Bharat Group"><img src="{{ asset('img/logo-bharattea.png') }}" data-rjs="{{ asset('img/logo-bharattea.png') }}" alt="Bharat Group" class="logo-light default"></a>
                </div>
                <!-- end logo -->
                <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu">
                    <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                        <span class="sr-only">toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                        <ul class="nav navbar-nav navbar-left panel-group no-margin alt-font font-weight-800">
                            <!-- <li><a href="#about" title="About" class="inner-link">@lang('general.about')</a></li> -->
                            <li><a href="{{ url('/#register') }}" title="Services" @isset($inner_link) class="inner-link" @endisset>@lang('general.register')</a></li>
                            <li><a href="{{ url('/#tnc') }}" title="Work" @isset($inner_link) class="inner-link" @endisset>@lang('general.tnc')</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- end navigation -->
</header>
<!-- end header -->
