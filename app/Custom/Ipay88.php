<?php

namespace Custom;

use Ixudra\Curl\Facades\Curl;
use App\LogIpay88;

class Ipay88
{
	public $data;

	public function prepare()
	{

		$this->data['MerchantCode'] = config('ipay88.merchant_code');
    $this->data['ResponseURL'] = url(config('ipay88.response_path'));
    $this->data['BackendURL'] = url(config('ipay88.backend_path'));
    $this->data['Lang'] = config('ipay88.encoding');
		$this->data['Currency'] = config('ipay88.default_currency');

		$this->data['signature'] = $this->signature(config('ipay88.merchant_key') . $this->data['MerchantCode'] . $this->data['RefNo'] . str_replace(array('.',','), '', $this->data['Amount']) . $this->data['Currency']);

    LogIpay88::create([
			'loi8_item' => $this->data['Item'],
			'loi8_ref' => $this->data['RefNo'],
			'loi8_request' => serialize($this->data),
		]);

		return $this->data;

	}


	public function signature($source)
	{
	       return base64_encode($this->hex2bin(sha1($source)));
	}

	private function hex2bin($hexSource)
	{
		$bin = '';
		for ($i=0;$i<strlen($hexSource);$i=$i+2)
		{
		       $bin .= chr(hexdec(substr($hexSource,$i,2)));
		}
		return $bin;
	}
}
