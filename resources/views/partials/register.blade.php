<h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.register')</h5>
*All fields are required to complete the registration.
<form-ajax action = "{{ url('register') }}" method="POST" @startwait="startWait" @endwait="endWait" suppress-success=1 @completesuccess="redirectSuccess">
<!-- {!! Form::open(['url' => 'register']) !!} -->
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-6">
      {{ Form::label('pro_name', trans('profile.pro_name') . ' / Nama') }}
      {{ Form::text('pro_name', null, ['class' => 'bg-transparent big-input']) }}
    </div>
    <div class="col-md-6">
      {{ Form::label('pro_ic', trans('profile.pro_ic') . ' / No IC/Passport') }}
      {{ Form::text('pro_ic', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      {{ Form::label('pro_dob', trans('profile.pro_dob') . ' / Tarikh lahir') }}
      <date-picker @update-date="updateDate" date-format="dd-mm-yy" v-once readonly class="bg-transparent  big-input"></date-picker>
      {{ Form::hidden('pro_dob', null, ['v-model' => 'pro_dob']) }}
    </div>
    <div class="col-md-6">
      {{ Form::label('pro_sex', trans('profile.pro_sex') . ' / Jantina') }}
      <div class="select-style big-select">
        {{ Form::select('pro_sex', $genders, null, ['class' => 'bg-transparent no-margin-bottom', 'v-model' => 'pro_sex']) }}
      </div>
    </div>
  </div>
  <div class="row">
  </div>
  <div class="row">
    <div class="col-md-6">
      {{ Form::label('pro_email', trans('profile.pro_email') . ' / Emel') }}
      {{ Form::email('pro_email', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
    <div class="col-md-6">
      {{ Form::label('pro_phone', trans('profile.pro_phone') . ' / No telefon') }}
      {{ Form::text('pro_phone', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
  </div>
  <div class="row">
  </div>
  <div class="row">
    <div class="col-md-12">
      {{ Form::label('pro_address', trans('profile.pro_address') . ' / Alamat rumah') }}
      {{ Form::text('pro_address', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      {{ Form::label('pro_zip', trans('profile.pro_zip') . ' / Poskod') }}
      {{ Form::text('pro_zip', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
    <div class="col-md-4">
      {{ Form::label('pro_city', trans('profile.pro_city') . ' / Bandar') }}
      {{ Form::text('pro_city', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
    <div class="col-md-4">
      {{ Form::label('pro_state', trans('profile.pro_state') . ' / Negeri') }}
      <div class="select-style big-select">
        {{ Form::select('pro_state', $states, null, ['class' => 'bg-transparent no-margin-bottom']) }}
      </div>
    </div>
  </div>
  <span class="text-extra-large display-block alt-font text-extra-dark-gray margin-10px-bottom font-weight-600">Next of kin (for emergency) / Waris terdekat (untuk kecemasan)</span>
  <div class="row">
    <div class="col-md-12">
      {{ Form::label('pro_emergency_name', trans('profile.pro_emergency_name') . ' / Nama') }}
      {{ Form::text('pro_emergency_name', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      {{ Form::label('pro_emergency_relation', trans('profile.pro_emergency_relation') . ' / Hubungan') }}
      {{ Form::text('pro_emergency_relation', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
    <div class="col-md-6">
      {{ Form::label('pro_emergency_contact', trans('profile.pro_emergency_contact') . ' / No telefon') }}
      {{ Form::text('pro_emergency_contact', null, ['class' => 'bg-transparent  big-input']) }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div v-show="category">
        {{ Form::label('pro_category', trans('profile.pro_category') . ' / Kategori penyertaan') }}
        {{ Form::hidden('pro_category', null, ['v-model' => 'pro_category']) }}
        <div class="text-extra-large"><b>@{{ category }}</b></div>
        <div class="text-large">@{{ price }}</div>
      </div>
      @include('partials.offline_payment')
    </div>
    <div class="col-md-6">
      {{ Form::label('pro_size', trans('profile.pro_size') . ' / Saiz kemeja-T') }}
      <a class="single-image-lightbox" href="{{ asset('img/sizes.jpg') }}"><i class="fa fa-question"></i> <img src="{{ asset('img/poster.jpg') }}" width=0></a> (M, L, XL)
      <div class="select-style big-select">
        {{ Form::select('pro_size', $sizes, null, ['class' => 'bg-transparent no-margin-bottom']) }}
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      {{ Form::submit(trans('general.button_register'), ['class' => 'btn btn-transparent-dark-gray btn-large margin-20px-top', '@click'=>"processWait", ':disabled' => 'waiting']) }}
    </div>
  </div>
<!-- {!! Form::close() !!} -->
</form-ajax>

@prepend('scripts')
<script>
Vue.component('date-picker', {
  template: '<input/>',
  props: [ 'dateFormat' ],
  mounted: function() {
  var self = this;
  $(this.$el).datepicker({
    dateFormat: this.dateFormat,
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0",
    onSelect: function(date) {
      self.$emit('update-date', date);
    }
  });
  },
  beforeDestroy: function() {
    $(this.$el).datepicker('hide').datepicker('destroy');
  }
});

  new Vue({
    el: '#register',

    data: {
      pro_dob: null,
      pro_sex: null,
      category: 'TBD',
      price: null,
      pro_category: null,
    },

    mixins: [mixForm],

    watch: {
      pro_dob: function () {
        this.updateCategory()
      },

      pro_sex: function () {
        this.updateCategory()
      },
    },

    created: function () {
    },

    methods: {

      updateCategory: function () {

        if (this.pro_dob != null && this.pro_sex != null) {

          this.$http.get("{{ url("api/v1/category/") }}/" + this.pro_dob + "/" + this.pro_sex)
              .then(function (response) {
                var data = JSON.parse(response.data)
                console.log(data)
                this.pro_category = data.cat_code
                this.category =  data.cat_code + " - " + data.cat_name
                this.price = "RM" + data.cat_price
              });

        }

      },

      updateDate: function(date) {
        this.pro_dob = date;
      },

      redirectSuccess: function (response) {

        var data = JSON.parse(response.data)

        var regid = data.data

        console.log(regid)

        window.location.replace('{{ url('register/success/') }}/' + regid)

      }
    }
  });


  $(function() {
    // $('.datepicker').datepicker({
    //   format: 'dd-mm-yyyy',
    //   autoclose: true,
    // });
    //
    // $('.datetimepicker').datetimepicker({
    //   format: 'DD-MM-YYYY HH:mm',
    // });
    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-top-right",
    }


  });

</script>
@endprepend
