<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cache;

class Code extends Model
{
  public static function dropDown($group, $skip_null = false)
  {
    // Cache::flush();

    return Cache::remember("code.$group", 90, function() use($group, $skip_null)
    {
      $return = [];

      if (!$skip_null) {

        $return = [''=> trans('general.select_any') ];

      }

      return $return + array_column(Code::where('cod_group', '=', $group)->where('cod_status', '=', 1)->orderBy('cod_order', 'asc')->orderBy('cod_name', 'asc')->get()->toArray(), 'cod_name', 'cod_key');
    });

  }
}
