<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Carbon\Carbon;
use App\Category;

class ApiController extends Controller
{

  public function category($dob, $sex)
  {
    if(Carbon::createFromFormat('d-m-Y', $dob) !== false) {

      $age = Carbon::parse($dob)->age;

      if ($age < 18) {

        $group = 'J';

      } else if ($age > 45) {

        $group = 'V';

      } else {

        $group = 'A';

      }

      return Category::where('cat_agegroup', '=', $group)->where('cat_gender', '=', $sex)->first();

    }
  }

}
