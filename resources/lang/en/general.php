<?php

return [

    'select_any' => 'Select',
    'register' => 'Register For Run',
    'about' => 'About Cherish',
    'tnc' => 'Terms & Conditions',
    'payment' => 'Payment',
    'payment_success' => 'Your payment was successful',
    'register_success' => 'We have received your registration. Kindly proceed to make the payment as below to confirm your entry.',
    'thank_you' => 'Thank you for your support!',
    'age' => 'Age',

    'button_register' => 'Register',
    'button_payment' => 'Proceed to Payment',
    'button_retry_payment' => 'Retry',
];
