<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'pro_id' => 'Reg No',
    'pro_name' => 'Name',
    'pro_email' => 'Email',
    'pro_phone' => 'Phone',
    'pro_dob' => 'Date of birth',
    'pro_sex' => 'Gender',
    'pro_ic' => 'IC/Passport no',
    'pro_address' => 'Home address',
    'pro_zip' => 'Post code',
    'pro_city' => 'City',
    'pro_state' => 'State',
    'pro_emergency_name' => 'Emergency name',
    'pro_emergency_relation' => 'Relationship',
    'pro_emergency_contact' => 'Emergency contact',
    'pro_category' => 'Category',
    'pro_size' => 'T-shirt size',
    'pro_status' => 'Payment status',
    'created_at' => 'Registered at',
];
