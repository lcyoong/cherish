<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <title>Run For Cherish | Bharat Group</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta property="og:url"           content="{{ url('/') }}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Run For Cherish | Bharat Group" />
  <meta property="og:description"   content="Charity run for C.H.E.R.I.S.H." />
  <meta property="og:image"         content="{{ asset('img/poster.jpg') }}" />

  <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
  <!-- animation -->
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
  <!-- bootstrap -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
  <!-- et line icon -->
  <link rel="stylesheet" href="{{ asset('css/et-line-icons.css') }}" />
  <!-- font-awesome icon -->
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />
  <!-- themify icon -->
  <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
  <!-- swiper carousel -->
  <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
  <!-- justified gallery  -->
  <link rel="stylesheet" href="{{ asset('css/justified-gallery.min.css') }}">
  <!-- magnific popup -->
  <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" />
  <!-- revolution slider -->
  <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}" media="screen" />
  <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/navigation.css') }}">
  <!-- bootsnav -->
  <link rel="stylesheet" href="{{ asset('css/bootsnav.css') }}">
  <!-- style -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
  <!-- responsive css -->
  <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />
  <!--[if IE]>
   <script src="{{ asset('js/html5shiv.js') }}"></script>
  <![endif]-->
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="{{ asset('build/toastr/toastr.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />

  <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
      ]); ?>
  </script>

  <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b1a8d2a74ac940011c15733&product=social-ab' async='async'></script>

</head>
    <body>
      @include('partials.header')
        @yield('content')
        @include('partials.footer')
        <!-- javascript libraries -->
        <script src="{{ asset(mix('js/app.js')) }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/modernizr.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/smooth-scroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
        <!-- menu navigation -->
        <script type="text/javascript" src="{{ asset('js/bootsnav.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.nav.js') }}"></script>
        <!-- animation -->
        <script type="text/javascript" src="{{ asset('js/wow.min.js') }}"></script>
        <!-- page scroll -->
        <script type="text/javascript" src="{{ asset('js/page-scroll.js') }}"></script>
        <!-- swiper carousel -->
        <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
        <!-- counter -->
        <script type="text/javascript" src="{{ asset('js/jquery.count-to.js') }}"></script>
        <!-- parallax -->
        <script type="text/javascript" src="{{ asset('js/jquery.stellar.js') }}"></script>
        <!-- magnific popup -->
        <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
        <!-- portfolio with shorting tab -->
        <script type="text/javascript" src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
        <!-- images loaded -->
        <script type="text/javascript" src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <!-- pull menu -->
        <script type="text/javascript" src="{{ asset('js/classie.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/hamburger-menu.js') }}"></script>
        <!-- counter  -->
        <script type="text/javascript" src="{{ asset('js/counter.js') }}"></script>
        <!-- fit video  -->
        <script type="text/javascript" src="{{ asset('js/jquery.fitvids.js') }}"></script>
        <!-- equalize -->
        <script type="text/javascript" src="{{ asset('js/equalize.min.js') }}"></script>
        <!-- skill bars  -->
        <script type="text/javascript" src="{{ asset('js/skill.bars.jquery.js') }}"></script>
        <!-- justified gallery  -->
        <script type="text/javascript" src="{{ asset('js/justified-gallery.min.js') }}"></script>
        <!--pie chart-->
        <script type="text/javascript" src="{{ asset('js/jquery.easypiechart.min.js') }}"></script>
        <!-- instagram -->
        <script type="text/javascript" src="{{ asset('js/instafeed.min.js') }}"></script>
        <!-- retina -->
        <script type="text/javascript" src="{{ asset('js/retina.min.js') }}"></script>
        <!-- revolution -->
        <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ asset('build/toastr/toastr.min.js') }}"></script>
        @stack('scripts')

    </body>
</html>
