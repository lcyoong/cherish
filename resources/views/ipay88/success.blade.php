@extends('layouts.master')
@section('content')
<section class="wow fadeIn xs-header-margin-top">
  <div class="container">
    <h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.thank_you')</h5>
		<h6>@lang('general.payment_success')</h6>
		Please check your email for the registration details. We look forward to meeting you on the day of the run.
	</div>
</section>
@stop
