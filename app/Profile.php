<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Profile extends Model
{
  protected $primaryKey = 'pro_id';

  protected $fillable = ['pro_name', 'pro_email', 'pro_phone', 'pro_dob', 'pro_ic', 'pro_sex', 'pro_address', 'pro_zip', 'pro_city', 'pro_state', 'pro_emergency_name', 'pro_emergency_relation', 'pro_emergency_contact', 'pro_category', 'pro_status', 'pro_size'];

  public function setProDobAttribute($value)
  {
    $this->attributes['pro_dob'] = empty($value) ? null : Carbon::parse($value)->format('Ymd');
  }

  public function category()
  {
    return $this->belongsTo(Category::class, 'pro_category');
  }
}
