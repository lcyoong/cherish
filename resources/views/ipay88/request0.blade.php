@extends('layouts.master')
@section('content')

<section class="wow fadeIn">
  <div class="container">
		<h1><div id="countdown"> </div></h1>
		Please do not refresh the page while being redirected.
		<form action="{{ env('IPAY88_ACTIVE') ? config('ipay88.request_url') : config('ipay88.mock_request_url') }}" method="post" name="request">
		<?php
		foreach ($data as $key => $value) {
			echo "<input type='hidden' name='".htmlentities($key)."' value='".htmlentities($value)."'>";
		}
		?>
		{{ Form::submit(trans('general.button_payment'), ['class' => 'btn btn-transparent-dark-gray btn-large margin-20px-top']) }}
		</form>
	</div>
</section>
	<script language="JavaScript">
	window.onload = function() {
	    var countdownElement = document.getElementById('countdown'),
	        seconds = 5,
	        second = 0,
	        interval;

	    interval = setInterval(function() {
	        countdownElement.firstChild.data = '@lang('form.redirecting') ' + (seconds - second) + ' seconds';
	        if (second >= seconds) {
	            // document.request.submit();
	            clearInterval(interval);
	        }

	        second++;
	    }, 1000);
	}
	</script>
@stop
