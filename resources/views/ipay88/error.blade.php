@extends('layouts.master')
@section('content')
<section class="wow fadeIn xs-header-margin-top">
  <div class="container">
    <h5 class="alt-font font-weight-700 text-extra-dark-gray text-uppercase margin-15px-bottom">@lang('general.payment')</h5>
		The following error was encountered during payment. If you keep encountering this issue, please contact us via <a href="mailto:{{ config('business.email') }}"><b>{{ config('business.email') }}</b></a> or <b>{{ config('business.tel1') }} / {{ config('business.tel2') }}</b>
    <div class="row">
      <div class="col-md-12">
        <h6>{{$err_desc}}</h6>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <a href="{{ url("payment/$item") }}">{{ Form::button(trans('general.button_retry_payment'), ['class' => 'btn btn-transparent-dark-gray btn-large margin-20px-top']) }}</a>
      </div>
    </div>
	</div>
</section>
@stop
