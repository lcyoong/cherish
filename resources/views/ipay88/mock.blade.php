@extends('components.open')
@section('content_panel')
	<h1>Mocking IPay88 <div id="countdown"> </div></h1>
	Please do not refresh the page while being redirected.
	<form action="{{ $ipay88['ResponseURL'] }}" method="post" name="request">
    <input type="hidden" name="MerchantCode" value="{{ $ipay88['MerchantCode'] }}"/>
    <input type="hidden" name="PaymentId" value="{{ $ipay88['PaymentId'] }}"/>
    <input type="hidden" name="RefNo" value="{{ $ipay88['RefNo'] }}"/>
    <input type="hidden" name="Amount" value="{{ $ipay88['Amount'] }}"/>
    <input type="hidden" name="Currency" value="{{ $ipay88['Currency'] }}"/>
    <input type="hidden" name="Remark" value="{{ $ipay88['Remark'] }}"/>
    <input type="hidden" name="TransId" value="{{ $ipay88['TransId'] }}"/>
    <input type="hidden" name="AuthCode" value="{{ $ipay88['AuthCode'] }}"/>
    <input type="hidden" name="Status" value="{{ $ipay88['Status'] }}"/>
    <input type="hidden" name="ErrDesc" value="{{ $ipay88['ErrDesc'] }}"/>
    <input type="hidden" name="Signature" value="{{ $ipay88['MockSignature'] }}"/>
    <input type="hidden" name="CCName" value="{{ $ipay88['CCName'] }}"/>
    <input type="hidden" name="CCNo" value="{{ $ipay88['CCNo'] }}"/>
    <input type="hidden" name="S_bankname" value="{{ $ipay88['S_bankname'] }}"/>
    <input type="hidden" name="S_country" value="{{ $ipay88['S_country'] }}"/>
    <input type="hidden" name="TokenId" value="{{ $ipay88['TokenId'] }}"/>
    <input type="hidden" name="BindCardErrDesc" value=""/>
    <input type="hidden" name="PANTrackNo" value=""/>
    <input type="hidden" name="ActionType" value=""/>
	</form>
	<script language="JavaScript">
	window.onload = function() {
	    var countdownElement = document.getElementById('countdown'),
	        seconds = 3,
	        second = 0,
	        interval;

	    interval = setInterval(function() {
	        countdownElement.firstChild.data = '@lang('form.redirecting') ' + (seconds - second) + ' seconds';
	        if (second >= seconds) {
	            document.request.submit();
	            clearInterval(interval);
	        }

	        second++;
	    }, 1000);
	}
	</script>
@stop
