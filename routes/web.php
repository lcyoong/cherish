<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/register', 'FrontController@index');
Route::get('register/success/{id}', 'FrontController@success');
Route::post('register', 'FrontController@register');
// Route::get('payment/success', 'PaymentController@success');
// Route::get('payment/{profile}', 'PaymentController@request');
// Route::post('payment/response', 'PaymentController@response');
