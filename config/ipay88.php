<?php

//--Configuration for status values
return array(
	//'merchant_code' => 'M00579',
	'merchant_code' => 'M05792',
	//'merchant_key' => '6xvb3I92q2',
	'merchant_key' => 'yKdZSuSyTI',
	'request_url' => 'https://payment.ipay88.com.my/epayment/entry.asp',
	'mock_request_url' => 'payment/request/mock',
	'encoding' => 'UTF-88',
	'response_path' => 'payment/response',
	'backend_path' => 'payment/backend',
	'backend_ok' => 'RECEIVEOK',
	'refresh_time' => 8000, //--1000 = 1 sec
	'processing_percent' => 1.6,
	'default_currency' => 'MYR',
);
