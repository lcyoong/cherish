<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogIpay88 extends Model
{
  protected $primaryKey = 'loi8_id';

  protected $fillable = ['loi8_item', 'loi8_ref', 'loi8_transid', 'loi8_errdesc', 'loi8_paymentid', 'loi8_request', 'loi8_response', 'loi8_verified', 'loi8_status', 'loi8_errstatus', 'loi8_trandate'];
}
